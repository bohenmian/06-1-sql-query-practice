/*
 * 请告诉我所有订单（order）中的订单明细的最大数目、最小数目和平均数目。结果应当包含三列：
 *
 * +────────────────────+────────────────────+────────────────────+
 * | minOrderItemCount  | maxOrderItemCount  | avgOrderItemCount  |
 * +────────────────────+────────────────────+────────────────────+
 */
SELECT MIN(oo.ordercount) minOrderItemCount,
       MAX(oo.ordercount) maxOrderItemCount,
       ROUND(AVG(oo.ordercount), 0) avgOrderItemCount
FROM (
         SELECT COUNT(orderdetails.orderNumber) AS ordercount
         FROM orders,
              orderdetails
         WHERE orders.orderNumber = orderdetails.orderNumber
         GROUP BY orderdetails.orderNumber
     ) AS oo;




