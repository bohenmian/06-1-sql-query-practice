/*
 * 请告诉我所有员工（employee）中有管理者（manager）的员工姓名及其管理者的姓名。所有的姓名
 * 请按照 `lastName, firstName` 的格式输出。例如：`Bow, Anthony`：
 *
 * +───────────+──────────+
 * | employee  | manager  |
 * +───────────+──────────+
 *
 * 输出结果按照 `manager` 排序，然后按照 `employee` 排序。
 */

SELECT CONCAT_WS(', ', e1.lastName, e1.firstName) as employee,
       CONCAT_WS(', ', e2.lastName, e2.firstName) as manager
FROM employees e1
         INNER JOIN employees e2
                    ON e1.reportsTo = e2.employeeNumber
ORDER BY manager, employee;
