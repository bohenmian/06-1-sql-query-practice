/*
 * 请告诉我总金额大于 60000 的每一个订单（`order`）编号及其总金额。查询结果应当包含如下信息：
 *
 * +──────────────+─────────────+
 * | orderNumber  | totalPrice  |
 * +──────────────+─────────────+
 *
 * 其结果应当以 `orderNumber` 排序。
 */

SELECT o.orderNumber, o.totalPrice
FROM (SELECT orders.orderNumber, SUM(orderdetails.quantityOrdered * orderdetails.priceEach) AS totalPrice
      FROM orders,
           orderdetails
      WHERE orders.orderNumber = orderdetails.orderNumber
      GROUP BY orders.orderNumber
     ) AS o
WHERE o.totalPrice > 60000;


