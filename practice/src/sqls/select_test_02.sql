/*
 * 请告诉我 `employeeNumber` 最大的 employee 的如下信息：
 *
 * +─────────────────+────────────+───────────+
 * | employeeNumber  | firstName  | lastName  |
 * +─────────────────+────────────+───────────+
 */
SELECT e.employeeNumber, e.firstName, e.lastName
FROM employees e
ORDER BY e.employeeNumber DESC
LIMIT 1;

